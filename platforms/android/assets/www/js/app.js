
var seat = angular.module('starter', ['ionic','categoryTree','ngMap','photosphere','starter.menu.controllers','starter.galleria','pascalprecht.translate','ngCordova','ion-gallery', 'starter.controllers', 'trustedFilter','starter.galleria'])

    seat.run(function($ionicPlatform,$translate) {

      $ionicPlatform.ready(function() {
        var  x = window.navigator.language||navigator.browserLanguage;
        var idioma = x.split('-');
        console.log(x);
        $translate.use(idioma[0]);
        console.log("probando " + idioma[0]);


        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
      });
    })

    seat.config(function($stateProvider, $urlRouterProvider,ionGalleryConfigProvider, $ionicConfigProvider,$translateProvider) {

        $translateProvider.useSanitizeValueStrategy('sanitizeParameters')
        $translateProvider.translations('en', {
            WELCOME: 'Welcome',
            TheEvent:'The event',
          Username:'Email',
          Password:'Password',
            Programme: 'Programme',
            StaticPictures: 'Static Pictures',
            DynamicPictures: 'Dynamic Pictures',
            EventPictures: 'Event Pictures',
            TechnicalInfo:' Technical Info',
            Documents:'Documents',
            VideoGallery:'Video Gallery',
            HowtoArrive:'How to Arrive',
            Weather:'Weather',
            TitleLogin:'Login',
            BtnLogin:'Sing in',
            PointInt:'Point of interest',
          Phone:"Phone",
          GoMap:"Start route",
          ViwDoc:"View document",
          BtnClose:'Close',
          GoHomeMap:'Go to start'


        });
        $translateProvider.translations('de', {
            WELCOME: 'WILLKOMMEN',
            Programme: 'Programm',
            StaticPictures: 'Statishe Fotos',
            DynamicPictures: 'Dynamishe Fotos',
            EventPictures: 'Eventbilder',
            TechnicalInfo:'Technische Daten',
            Documents:'Unterlagen',
            VideoGallery: 'Videos',
            HowtoArrive:'Wegbeschreibung',
            Weather:'Wetter',
            TitleLogin:'Login',
            BtnLogin:'Eingeben',
          PointInt:'Point of interest',
          Username:'Email',
          Password:'Password',
          Phone:"Phone",
          GoMap:"Start route",
          ViwDoc:"View document",
          TheEvent:'The event',
          BtnClose:'schließen',
          GoHomeMap:"Nach oben"

        });
        $translateProvider.translations('it', {
            WELCOME: 'Benvenuto',
            Programme: 'Programma',
            StaticPictures: 'Foto Statiche',
            DynamicPictures: 'Foto Dinamiche',
            EventPictures: 'Event Pictures',
            TechnicalInfo:'Schede tecniche',
            Documents:'Documentazione',
            VideoGallery: 'Videos',
            HowtoArrive:'Indicazioni',
            Weather:'Tempo',
            TitleLogin:'Login',
            BtnLogin:'Entrare',
            PointInt:'Punto di interesse',
            Username:'Email',
            Password:'Password',
          Phone:"Phone",
          GoMap:"Start route",
          ViwDoc:"View document",
          TheEvent:'The event',
          BtnClose:'Vicino',
          GoHomeMap:"Andare per iniziare"

        });

        $translateProvider.translations('fr', {
            WELCOME: 'Levenement',
            Programme: 'Programme',
            StaticPictures: 'Photos fixes',
            DynamicPictures: 'Photos dynamiques',
            EventPictures: 'Event Pictures',
            TechnicalInfo:'Fiches de donnees',
            Documents:'Documents',
            VideoGallery: 'Videos',
            HowtoArrive:' Coment Arriver',
            Weather:'Temps',
            TitleLogin:'Login',
            BtnLogin:'Entrer',
            PointInt:"Point d'intérêt",
          Username:'Email',
          Password:'Password',
          Phone:"Phone",
          Login:"iniciar sesión",
          GoMap:"Start route",
          ViwDoc:"View document",
          TheEvent:'The event',
          BtnClose:'Fermer',
          GoHomeMap:"aller à commencer"
        });


        $translateProvider.translations('es', {
            WELCOME: 'BIENVENIDO',
            Programme: 'Agenda',
            StaticPictures: "Fotos estáticas",
            DynamicPictures: "Fotos dinámicas",
            EventPictures: 'Fotos del evento',
            TechnicalInfo:"Fichas técnicas",
            Documents:'Documentos',
            VideoGallery: "Vídeos",
            HowtoArrive:' Como llegar',
            Weather:'Clima',
            TitleLogin:"Inicio de Sessión",
            BtnLogin:'Acceder',
            PointInt:"Puntos de Interés",
          Username:'Email',
          Password:'Contraseña',
          Phone:"Teléfonos",
          Login:"Iniciar sesión",
          GoMap:"Iniciar ruta",
          ViwDoc:"Ver documento",
          TheEvent:'EL EVENTO',
          BtnClose:'Cerrar',
          GoHomeMap:"Llevame al inicio"

        });


        $translateProvider.preferredLanguage('en');


ionGalleryConfigProvider.setGalleryConfig({
        action_label: 'Close',
        actionShare:'Shares',
        toggle: false,
        row_size: 3,
        fixed_row_size: true
      });



    //CON ESTO CONSEGUIMOS QUE EL TÍTULO ESTÉ CENTRADO EN ANDROID, ES NECESARIO AÑADIR '$ionicConfigProvider' COMO PARÁMETRO EN LA FUNCTION DEL CONFIG
    $ionicConfigProvider.navBar.alignTitle("center");
      $ionicConfigProvider.backButton.previousTitleText(false);
      $ionicConfigProvider.backButton.icon('ion-chevron-left');
      $ionicConfigProvider.backButton.text('')
      //$ionicConfigProvider.backButton.text('').icon('ion-ios7-arrow-left');

        $stateProvider
            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/menu.html",
                controller: 'CategoryCtrl'
            })

            .state('app.agenda', {
                url: "/agenda",
                views: {
                    'menuContent': {
                        templateUrl: "templates/agenda.html",
                        controller: 'AgendaCtrl'
                    }
                }
            })
            .state('app.home', {
                url: "/home",
                views: {
                    'menuContent': {
                        templateUrl: "templates/home.html",
                        controller: 'DashCtrl'
                    }
                }
            })
            .state('app.dinamicas', {
                url: "/dinamicas",
                views: {
                    'menuContent': {
                        templateUrl: "templates/dinamicas.html",
                        controller: 'GalleryCtrl'
                    }
                }
            })

            .state('app.estaticas', {
                url: "/estaticas",
                views: {
                    'menuContent': {
                        templateUrl: "templates/estaticas.html",
                        controller: 'GalleryStaticCtrl'
                    }
                }
            })

            .state('app.eventos', {
                url: "/eventos",
                views: {
                    'menuContent': {
                        templateUrl: "templates/eventos.html",
                        controller: 'GalleryEventCtrl'
                    }
                }
            })

            .state('app.videos', {
                url: "/videos",
                views: {
                    'menuContent': {
                        templateUrl: "templates/videos.html",
                        controller: 'VideoGalleryCtrl'
                    }
                }

            })
            .state('app.videoplayer', {
                url: "/videos/:id",
                views: {
                    'menuContent': {
                        templateUrl: "templates/videoPlayer.html",
                        controller: 'VideoPlayerCtrl'
                    }
                }
            })

            .state('app.fichatecnica', {
                url: "/fichatecnica",
                views: {
                    'menuContent': {
                        templateUrl: "templates/fichatecnica.html",
                        controller: 'PdfCtrl'
                    }
                }
            })

            .state('app.doc', {
                url: "/doc",
                views: {
                    'menuContent': {
                        templateUrl: "templates/doc.html",
                        controller: 'DocCtrl'
                    }
                }
            })



            .state('app.360', {
                url: "/360",
                views: {
                    'menuContent': {
                        templateUrl: "templates/360.html",
                        controller: 'ListDashCtrl'
                    }
                }
            })

            .state('app.view360', {
                url: "/view360/:imgUrl",
                views: {
                    'menuContent': {
                        templateUrl: "templates/view360.html",
                        controller: 'View360Ctrl'
                    }
                }
            })

            .state('app.times', {
                url: "/times",
                views: {
                    'menuContent': {
                        templateUrl: "templates/times.html",
                        controller: 'WeatherCtrl'
                    }
                }
            })

            .state('app.weather', {
                url: "/weather",
                views: {
                    'menuContent': {
                        templateUrl: "templates/weather.html",
                        controller: 'WeatherCtrl'
                    }
                }
            })

            .state('app.map', {
                url: "/map",
                views: {
                    'menuContent': {
                        templateUrl: "templates/howToArrive.html",
                        controller: 'MapController'
                    }
                }
            })

            .state('app.lisevento', {
                url: "/lisevento",
                views: {
                    'menuContent': {
                        templateUrl: "templates/liseventos.html",
                        controller: 'ListEventCtrl'
                    }
                }
            })

            .state('app.kml', {
                url: "/kml/:urlKml",
                views: {
                    'menuContent': {
                        templateUrl: "templates/kml.html",
                        controller: 'KmlView'
                    }
                }
            })

            .state('app.phone', {
                url: "/phone",
                views: {
                    'menuContent': {
                        templateUrl: "templates/phone.html",
                        controller: 'PhoneCtrl'
                    }
                }
            })
            .state('app.punto', {
            url: "/punto",
            views: {
                'menuContent': {
                    templateUrl: "templates/punto.html",
                    controller: 'PuntoCtrl'
                }
            }
        })

            .state('app.karpano', {
                url: "/karpano",
                views: {
                    'menuContent': {
                        templateUrl: "templates/karpano.html",
                        controller: 'PuntoCtrl'
                    }
                }
            })

          .state('app.login', {
            url: "/login",
            views: {
              'menuContent': {
                templateUrl: "templates/modallogin.html",
                controller: 'CtrlLgogin'
              }
            }
          })

          .state('app.mygalleria', {
                url: "/mygalleria",
                views: {
                    'menuContent': {
                        templateUrl: "templates/mygalleria.html",
                        controller: 'GalleryStaticCtrl'
                    }
                }
            })

          .state('app.logueado', {
            url: "/logueado",
            views: {
              'menuContent': {
                templateUrl: "templates/logueado.html",
                controller: 'CtrlLgogin'
              }
            }
          })

          .state('app.listadokml', {
            url: "/listadokml",
            views: {
              'menuContent': {
                templateUrl: "templates/listakml.html",
                controller: 'KmlCtrl'
              }
            }
          })

          .state('app.viewpunto',{
            url:"/viewpunto",
            views:{
              "menuContent":{
                templateUrl:"templates/viewpunto.html",
                controller: 'PuntoCtrl'
              }
            }
          })

          .state('app.viewvideo360',{
            url:"/viewvideo360",
            views:{
              "menuContent":{
                templateUrl:"templates/360video.html",
                controller: 'Video360Ctrl'
              }
            }
          })

          .state('app.listavideo360',{
            url:"/listavideo360",
            views:{
              "menuContent":{
                templateUrl:"templates/listavideos.html",
                controller: 'Video360Ctrl'
              }
            }
          })


      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/app/home');

    });
