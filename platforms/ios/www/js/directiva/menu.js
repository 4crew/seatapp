angular.module('starter.menu.controllers', [])


  .service('myService', ['$http', '$q', function($http, $q){
    var  x = window.navigator.language||navigator.browserLanguage;
    var idioma = x.split('-');
    console.log(x);
    var deferObject,
      myMethods = {

        getPromise: function() {
          var promise       =  $http.get('http://ws1.seatprapp.com/acciones/idiomas/'+idioma[0]+'.json'),
            deferObject =  deferObject || $q.defer();

          promise.then(
            // OnSuccess function
            function(answer){
              // This code will only run if we have a successful promise.
              deferObject.resolve(answer);
            },
            // OnFailure function
            function(reason){
              // This code will only run if we have a failed promise.
              deferObject.reject(reason);
            });

          return deferObject.promise;
        }
      };

    return myMethods;

  }])

  .factory('Category', function($filter) {

    var all = function() {
      return [

        {

          name: $filter('translate')('TheEvent'),
          id: 1,
          items: [// Required: array, even empty
            {
              name: $filter('translate')('Agenda'),
              url: 'app/agenda',
            },
            {
              name: $filter('translate')('StaticPictures'),
              url:"app/estaticas",
            },
            {
              name: $filter('translate')('DynamicPictures'),
              url: "app/dinamicas",

            },
            {
              name: $filter('translate')('EventPictures'),
              url:"app/eventos",
            },
            {
              name: $filter('translate')('TechnicalInfo'),
              url: "app/fichatecnica",

            }, {
              name: $filter('translate')('Documents'),
              url:"app/doc",
            },
            {
              name: "360",
              url: "app/360",

            }, {
              name: $filter('translate')('VideoGallery'),
              url:"app/videos"
            },
            {
              name: "Roadbooks",
              url: "app/listadokml",

            }, {
              name: $filter('translate')('PointInt'),
              url:"app/punto",
            },
            {
              name: $filter('translate')('Phone'),
              url: "app/phone",

            }

          ]
        },
        {
          name:$filter('translate')('TitleLogin'),
          url:"app/login"
        }
      ]
    }

    // Should be a DB query in real life
    var get = function(id) {
      var categories = all();

      for (var i = 0; i < categories.length; i++) {
        var level1 = categories[i];
        if (level1.id == id) {
          return level1;
        }

        for (var j = 0; j < level1.items.length; j++) {
          var level2 = level1.items[j];

          if (level2.id == id) {
            return level2;
          }

          for (var k = 0; k < level2.items.length; k++) {
            var level3 = level2.items[k];

            if (level3.id == id) {
              return level3;
            }
          }
        }
      }

      return null;
    }

    // Public API
    return {
      all: all,
      get: get
    }
  })

  .controller('AppCtrl', function($scope, Category) {
    $scope.categories = Category.all();
  })

  .controller('CategoryCtrl', function($scope, Category, $stateParams,myService) {
    $scope.estado = false;
    $scope.Open = function () {
      console.log('inicioando');
      if($scope.estado){
        $scope.estado = false;
        console.log('cerrando');
        var elemento = document.getElementById('iconseat');
        elemento.setAttribute("class","icon iconmenu ion-arrow-left-b");
      }else {
        $scope.estado = true;
        console.log('abierto');
        var elemento = document.getElementById('iconseat');
        elemento.setAttribute("class","icon iconmenu ion-arrow-down-b");
      }
    }

    $scope.category = Category.get($stateParams.id);
    var askForPromise = myService.getPromise();

    askForPromise.then(
      // OnSuccess function
      function(answer) {
        $scope.Imenu = answer.data;
        //$scope.success = true;
      },
      // OnFailure function
      function(reason) {
        console.log(reason);
        // $scope.error = true;
      }
    )
  });
