var seat = angular.module('starter.controllers', [])


seat.controller('DashCtrl', function($scope,$http,$ionicLoading,$timeout,$cordovaLocalNotification,$ionicModal) {



        $ionicModal.fromTemplateUrl('templates/modallogin.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.push = {
            id:'',
            message:''
        }

        function Getpush(){
            $http.post('http://ws1.seatprapp.com/acciones/push.php').then(function(resp){
              if(resp.data.length > 0){
                $scope.push.id = resp.data[0].idpush;
                $scope.push.message = resp.data[0].push_txt;

                $cordovaLocalNotification.schedule({
                  id: 1,
                  title: 'SEAT PR App',
                  text: $scope.push.message
                },function (result) {
                  console.log(result);
                });
              }

            console.log('no hay push');



            },function(error){

            })

        }



        $http.post('http://ws1.seatprapp.com/acciones/home.php').then(function(resp) {

              $scope.dash = resp;
              $scope.video = resp.data.video;
              $timeout(Getpush(),3000);




        }, function(err) {
            console.log('ERR', JSON.stringify(err));
        })
    })

seat.controller('ListEventCtrl', function($scope,$http,$state) {


        $http.post('http://ws1.seatprapp.com/acciones/changeeventos.php').then(function(resp) {

            $scope.listaevento = resp.data;
            console.log(JSON.stringify(resp.data));

        }, function(err) {
            console.log('ERR', JSON.stringify(err));
        });

        $scope.ChangeEvent = function (id) {

            //console.log(JSON.stringify(id));
            //alert(id);

            $http.get('http://ws1.seatprapp.com/acciones/eventosChange.php?id='+id).then(function(resp) {

                $scope.RequestData = resp.data;

                window.location.reload(true);
                $state.go('app.home');

            }, function(err) {
                console.log('ERR', JSON.stringify(err));
            })
        }
    })

seat.controller('ListDashCtrl', function($scope, $http,$state,$timeout) {




        $scope.goGetUrl =function (param){
            $state.go('app.view360',{imgUrl:param});
        }

        $scope.images =[];

        $http.post('http://ws1.seatprapp.com/acciones/360.php').then(function(resp) {
            $scope.images = resp.data;

            console.log('bien' + $scope.images.length);
        }, function(err) {
            console.log('ERR', JSON.stringify(err));
        })

    })

seat.controller('View360Ctrl', function($scope, $stateParams) {

        $scope.img = $stateParams.imgUrl;

    })

seat.controller('GalleryCtrl', function($scope,$http, $cordovaSocialSharing){
        $scope.items =[];

        $scope.onClick = function(){

            var urls = "";
            for(var i=0; i < $scope.items.length; i++){
                var url = $scope.items[i].src + "<br>";
                url = url.replace("tumb_","");
                urls += url;
            }

            var message = "<i><p>Here you have all the dynamic pictures used at Geneva 2016 SEAT event. Use the urls below (left-click to see picture on browser, right click and save as to download it): </p></i>" + urls;
            var subject = "SEAT Ateca - Geneve 2016 dynamic pictures";
            var toArr = [];
            var ccArr = [];
            var bccArr = [];
            var file = null;

            $cordovaSocialSharing
                .shareViaEmail(message, subject, toArr, ccArr, bccArr, file)
                .then(function(result) {
                    console.log("Success: " + result);
                }, function(err) {
                    console.error("Error: " + err);
                });
        }

        $http.post('http://ws1.seatprapp.com/acciones/dinamicas.php').then(function(resp) {
            for(var i=0; i< resp.data.length; i++){
                $scope.items.push(
                    {src:'http://seatprapp.com/backoffice/upload/info_eventos/dinamicas/app_'+resp.data[i].src,thumb:'http://seatprapp.com/backoffice/upload/info_eventos/dinamicas/tumb_'+resp.data[i].src}
                );
            }            console.log('bien' + $scope.items.length);
        }, function(err) {
            console.log('ERR', JSON.stringify(err));
        })
    })

seat.controller('GalleryStaticCtrl', function($scope,$http, $cordovaSocialSharing,$state){
        $scope.estaticas =[];
        $scope.miGalleria = [];
        $scope.onClick = function(){

            var urls = "";
            for(var i=0; i < $scope.estaticas.length; i++){
                var url = $scope.estaticas[i].src + "<br>";
                url = url.replace("tumb_","");
                urls += url;
            }

            var message = "<i><p>Here you have all the static pictures used at Geneva 2016 SEAT event. Use the urls below (left-click to see picture on browser, right click and save as to download it): </p></i>" + urls;
            var subject = "SEAT Ateca - Geneve 2016 static pictures";
            var toArr = [];
            var ccArr = [];
            var bccArr = [];
            var file = null;

            $cordovaSocialSharing
                .shareViaEmail(message, subject, toArr, ccArr, bccArr, file)
                .then(function(result) {
                    console.log("Success: " + result);
                }, function(err) {
                    console.error("Error: " + err);
                });
        }

        $http.post('http://ws1.seatprapp.com/acciones/estaticas.php').then(function(resp) {
            for(var i=0; i< resp.data.length; i++){
                $scope.estaticas.push(
                    {src:'http://seatprapp.com/backoffice/upload/info_eventos/estaticas/app_'+resp.data[i].src,thumb:'http://seatprapp.com/backoffice/upload/info_eventos/estaticas/tumb_'+resp.data[i].src}
                );
            }            //console.log('bien' + $scope.items.length);
        }, function(err) {
            console.log('ERR', JSON.stringify(err));
        })

  $scope.Ver =function (item) {
    alert(item);
  }


  $http.post('http://ws1.seatprapp.com/acciones/login.php').then(function(respuesta) {
    for(var j=0; j< respuesta.data.length; j++){
      $scope.miGalleria.push(
        {src:respuesta.data[j].src}
      );
    }            //console.log('bien' + $scope.items.length);
  }, function(err) {
    console.log('ERR', JSON.stringify(err));
  })



})

seat.controller('GalleryEventCtrl', function($scope,$http, $cordovaSocialSharing){
        $scope.events =[];

        $scope.onClick = function(){

            var urls = "";
            for(var i=0; i < $scope.events.length; i++){
                var url = $scope.events[i].src + "<br>";
                url = url.replace("tumb_","");
                urls += url;
            }

            var message = "<i><p>Here you have all the pictures from the SEAT Ateca 2016 event at Geneva. Use the urls below (left-click to see picture on browser, right click and save as to download it): </p></i>" + urls;
            var subject = "SEAT Ateca - Geneve 2016 event pictures";
            var toArr = [];
            var ccArr = [];
            var bccArr = [];
            var file = null;

            $cordovaSocialSharing
                .shareViaEmail(message, subject, toArr, ccArr, bccArr, file)
                .then(function(result) {
                    console.log("Success: " + result);
                }, function(err) {
                    console.error("Error: " + err);
                });
        }

        $http.post('http://ws1.seatprapp.com/acciones/eventos.php').then(function(resp) {
            for(var i=0; i< 20; i++){
                $scope.events.push(
                    {src:resp.data[i].img,thumb:resp.data[i].src}
                );
            }            //console.log('bien' + $scope.items.length);
        }, function(err) {
            console.log('ERR', JSON.stringify(err));
        })
    })

seat.controller('VideoGalleryCtrl', function($scope,$http, $cordovaSocialSharing){

        $scope.onClick = function(){
            //alert("URL del video: " + $scope.videoURL);

            //var links = "";
            var urls = "";
            for(var i=0; i < $scope.videos.length; i++){
                //links += "<a href='http://seatprapp.com/backoffice/upload/eventos/videos/" + $scope.videos[i].video + "_movil.mp4'>" + $scope.videos[i].video + "</a><br>";
                urls += $scope.videos[i].video + ": " + "http://seatprapp.com/backoffice/upload/eventos/videos/" + $scope.videos[i].video + "_movil.mp4<br>";
            }

            var message = "<i><p>Here you have all the videos from the SEAT Ateca 2016 event at Geneva. Use the urls below (left-click to see video on browser, right click and save as to download it): </p></i>" + urls;
            var subject = "SEAT Ateca - Geneve 2016 videos";
            var toArr = [];
            var ccArr = [];
            var bccArr = [];
            var file = null;

            $cordovaSocialSharing
                .shareViaEmail(message, subject, toArr, ccArr, bccArr, file)
                .then(function(result) {
                    console.log("Success: " + result);
                }, function(err) {
                    console.error("Error: " + err);
                });
        }

        $http.post('http://ws1.seatprapp.com/acciones/videos.php').then(function(resp) {
            $scope.videos = resp.data
            //console.log('bien' + $scope.items.length);
            console.log('bien' + $scope.videos);
        }, function(err) {
//    console.log('ERR', JSON.stringify(err));
            console.log('ERR', err);
        })

    })

seat.controller('VideoPlayerCtrl', function($scope,$http,$state, $cordovaSocialSharing){

        $scope.onClick = function(){
            //alert("URL del video: " + $scope.videoURL);

            var message = "<p>Here you have the link for the requested video from SEAT Ateca 2016 event at Geneva</p><a href='" + $scope.videoURL + "'>" + $scope.data.video  + ": </a>" + $scope.videoURL;
            var subject = $scope.data.video+ " video from SEAT Ateca - Geneve 2016";
            var toArr = [];
            var ccArr = [];
            var bccArr = [];
            var file = null;

            $cordovaSocialSharing
                .shareViaEmail(message, subject, toArr, ccArr, bccArr, file)
                .then(function(result) {
                    console.log("Success: " + result);
                }, function(err) {
                    console.error("Error: " + err);
                });
        }

        $http.post('http://ws1.seatprapp.com/acciones/videos.php').then(function(resp) {
            $scope.videoID = $state.params.id;
            $scope.data = resp.data[$state.params.id];
            $scope.videoURL = "http://seatprapp.com/backoffice/upload/eventos/videos/" + $scope.data.video + "_movil.mp4"

            //console.log('bien' + $scope.items.length);
            console.log('bien' + $scope.videoURL);
        }, function(err) {
            //console.log('ERR', JSON.stringify(err));
            console.log('ERR', err);
        })
    })

seat.controller('PdfCtrl',function($scope,$http) {

        $scope.openPDF = function(url){
            var ref = window.open(url, '_system', 'location=yes');
            return ref;
        }

        $scope.ficha = [];

        $http.post('http://ws1.seatprapp.com/acciones/ficha.php').then(function(resp) {
            for(var i=0; i< resp.data.length; i++){
                $scope.ficha.push(
                    {imagen:'http://seatprapp.com/backoffice/upload/info_eventos/coches/fotos/app_'+resp.data[i].imagen,
                        pdf:'http://seatprapp.com/backoffice/upload/info_eventos/coches/pdfs/'+resp.data[i].pdf,
                        modelo:resp.data[i].modelo}
                );
            }            console.log('bien' + $scope.items.length);
        }, function(err) {
            console.log('ERR', JSON.stringify(err));
        })

    })

seat.controller('DocCtrl',function($scope,$http) {

        $scope.openPDF = function(url){
            var ref = window.open(url, '_system', 'location=yes');
            return ref;
        }

        $scope.doc = [];

        $http.post('http://ws1.seatprapp.com/acciones/doc.php').then(function(resp) {
            for(var i=0; i< resp.data.length; i++){
                $scope.doc.push(
                    { pdf:'http://seatprapp.com/backoffice/upload/eventos/documentos/'+resp.data[i].documento,
                        name:resp.data[i].nombre}
                );
            }            console.log('bien' + $scope.items.length);
        }, function(err) {
            console.log('ERR', JSON.stringify(err));
        })
    })

seat.controller('WeatherCtrl',function($scope,$cordovaGeolocation, $http, $ionicLoading){

        var posOptions = {timeout: 10000, enableHighAccuracy: false};
        $cordovaGeolocation
            .getCurrentPosition(posOptions)
            .then(function (position) {

                var lat  = position.coords.latitude
                var long = position.coords.longitude
                var directions = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'];

                $scope.getIconUrl = function(iconId) {
                    return 'http://openweathermap.org/img/w/' + iconId + '.png';
                };
                $ionicLoading.show();


                $http.get('http://api.openweathermap.org/data/2.5/weather?lat='+ lat +'&lon='+ long +'&units=metric&appid=44db6a862fba0b067b1930da0d769e98').success(function (weather) {
                    $scope.weather = weather;
                    $ionicLoading.hide();
                    // alert(JSON.stringify(weather));
                }).error(function (err) {
                    $ionicLoading.show({
                        template: 'Could not load weather. Please try again later.',
                        duration: 3000
                    });
                });

                $scope.getDirection = function (degree) {
                    if (degree > 338) {
                        degree = 360 - degree;
                    }
                    var index = Math.floor((degree + 22) / 45);
                    return directions[index];
                };

            }, function(err) {
                alert(err);
            });

    })

seat.controller('MapController', function($scope, uiGmapGoogleMapApi) {


        $scope.controlClick = function () {
            window.open("https://www.google.es/maps/dir/'" + $scope.myLocation.lat + "," + $scope.myLocation.lng + "'/'" + $scope.seatLat + "," + $scope.seatLong + "'",'_system','location=yes');
        };


        $scope.controlClickRetry = function () {
            $scope.visibleLoading = true;
            $scope.visibleTakeMe  = false;
            $scope.visibleRetry   = false;

            navigator.geolocation.getCurrentPosition($scope.drawMap, $scope.handleError, $scope.options);

        };

        $scope.map = {};
        $scope.myLocation = {
            lng : '',
            lat : ''
        }

        $scope.visibleLoading   = false;
        $scope.visibleTakeMe    = false;
        $scope.visibleRetry     = true;
        $scope.refreshMap       = false;

        $scope.seatLong = 6.1171015;
        $scope.seatLat  = 46.2353841;

        $scope.map = {
            center: {
                latitude: $scope.seatLat,
                longitude: $scope.seatLong
            },
            zoom: 14,
            pan: 1,
            options : {
                scrollwheel: false,
            }
        };

//    $scope.seatLong = 6.1;
//    $scope.seatLat  = 46.2;

        var image = {
            url: 'img/seatMarker.png',
            scaledSize: {
                height: 32,
                width: 32
            }
        };


        $scope.marker2 = {
            id: 1,
            coords: {
                latitude: $scope.seatLat,
                longitude: $scope.seatLong
            },
            options: { icon: image}
        }

        $scope.marker = {
            id: 0,
            coords: {
                latitude: '',
                longitude: ''
            },
        }

        $scope.drawMap = function(position){

            $scope.visibleLoading = false;
            $scope.visibleTakeMe = true;
            $scope.visibleRetry = false;

            $scope.myLocation.lng = position.coords.longitude;
            $scope.myLocation.lat = position.coords.latitude;

            //TESTING ONLY
            //$scope.myLocation.lng = 6.217;
            //$scope.myLocation.lat = 46.230;

            var difLong = $scope.myLocation.lng - $scope.seatLong;
            var difLat  = $scope.myLocation.lat - $scope.seatLat;

            var zoomCalibration = 0;
            if(difLat > 0) zoomCalibration = 1;
            console.log(zoomCalibration);

            //CALCULATE DISTANCE BETWEEN GEOPOINTS
            var long1 = $scope.myLocation.lng/57.2958;
            var long2 = $scope.seatLong/57.2958;

            var lat1 = $scope.myLocation.lat/57.2958;
            var lat2 = $scope.seatLat/57.2958;

            var R = 6371;
            var x = (lat1 - lat2) * Math.cos((long1 + long2)/2);
            var y = (long1 - long2);
            var distance = R * Math.sqrt((x*x) + (y*y));
            $scope.distance = distance.toString();

            //CALCULATE ZOOM
            var zoom = 4;
            if(distance < 0.25) zoom = 17;
            else if(distance < 0.5) zoom = 16;
            else if(distance < 1) zoom = 15;
            else if(distance < 2) zoom = 14;
            else if(distance < 4) zoom = 13;
            else if(distance < 8) zoom = 12;
            else if(distance < 16) zoom = 11;
            else if(distance < 32) zoom = 10;
            else if(distance < 64) zoom = 9;
            else if(distance < 128) zoom = 8;
            else if(distance < 256) zoom = 7;
            else if(distance < 512) zoom = 6;
            else if(distance < 1024) zoom = 5;

            zoom -= zoomCalibration;

            //DEFINE MAP AND MARKERS
            $scope.map = {
                center: {
                    latitude: $scope.myLocation.lat - difLat/2,
                    longitude: $scope.myLocation.lng - difLong/2
                },
                zoom: zoom,
                pan: 1,
                options : {
                    scrollwheel: false,
                    mapTypeControl: true
                }
            };

            //MARKERS
            $scope.marker = {
                id: 0,
                coords: {
                    latitude: $scope.myLocation.lat,
                    longitude: $scope.myLocation.lng
                },
                label: "You are here"
            };

            //SETTING SEAT LOGO AS MARKER'S ICON
            var image = {
                url: 'img/seatMarker.png',
                scaledSize: {
                    height: 32,
                    width: 32
                }
            };


            $scope.marker2 = {
                id: 1,
                coords: {
                    latitude: $scope.seatLat,
                    longitude: $scope.seatLong
                },
                options: {
                    icon: image,
                }
            }

            $scope.windowOptions = {
                visible: false
            };

            $scope.windowOptions2 = {
                visible: false
            };

            $scope.closeClick = function() {
                $scope.windowOptions.visible = false;
            };

            $scope.onClick = function(){
                $scope.windowOptions.visible = !$scope.windowOptions.visible;
            };

            $scope.closeClick2 = function() {
                $scope.windowOptions2.visible = false;
            };

            $scope.onClick2 = function(){
                $scope.windowOptions.visible = !$scope.windowOptions.visible;
            };

            $scope.title = "SEAT stand";


            $scope.refreshMap = true;
            $scope.$apply();
        }

        $scope.handleError = function(error) {
            console.warn('ERROR(' + error.code + '): ' + error.message);

            $scope.visibleRetry = true;
            $scope.visibleLoading = false;
            $scope.visibleTakeMe = false;
            $scope.refreshMap = false;
            function alertDismissed() {

            }

            navigator.notification.alert(
                'If GPS is disabled, please enable it and try again in a few seconds.',  // message
                alertDismissed, // callback
                'Unable to get geolocation!', // title
                'Understood' // buttonName
            );

            $scope.$apply();
        }

        $scope.options = {
            enableHighAccuracy: true,
            timeout: 4000,
            maximumAge: 10000
        };



        uiGmapGoogleMapApi.then(function(maps) {
            $scope.mapControl = maps[0];
        });
    })

seat.controller('PhoneCtrl', function($scope,$http) {

                var  x = window.navigator.language||navigator.browserLanguage;
        var idioma = x.split('-');
        console.log(x);
           $http.get('http://ws1.seatprapp.com/acciones/telefonos.php?idioma='+idioma[0]).then(function (data) {
               $scope.ReqPhone= data.data;

           },function (error) {
               console.log(error);
           })




    })

seat.controller('PuntoCtrl',function ($scope,$http) {
        var  x = window.navigator.language||navigator.browserLanguage;
        var idioma = x.split('-');
        console.log(x);
        $http.get('http://ws1.seatprapp.com/acciones/interes.php?idioma='+idioma[0]).then(function (data) {
            $scope.ReqInt= data.data;

        },function (error) {
            console.log(error);
        })



    })



angular.element(document).ready(function() {
    angular.bootstrap(document, ['starter.controllers']);
});

