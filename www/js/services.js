angular.module('starter.servicesMap', [])

  .factory('GoogleMapsService', ['$rootScope', '$ionicLoading', '$timeout', '$window', '$document', 'ConnectivityService', function($rootScope, $ionicLoading, $timeout, $window, $document, ConnectivityService) {

    var apiKey = false,
      map = null,
      mapDiv = null,
      directionsService,
      directionsDisplay,
      routeResponse;

    function initService(mapEl, key) {
      mapDiv = mapEl;
      if (typeof key !== "undefined") {
        apiKey = key;
      }
      if (typeof google == "undefined" || typeof google.maps == "undefined") {
        disableMap();
        if (ConnectivityService.isOnline()) {
          $timeout(function() {
            loadGoogleMaps();
          }, 0);
        }
      } else {
        if (ConnectivityService.isOnline()) {
          initMap();
          enableMap();
        } else {
          disableMap();
        }
      }
    }

    function initMap() {
      if (mapDiv) {
        var mapOptions = {
          zoom: 10,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(mapDiv, mapOptions);
        directionsService = new google.maps.DirectionsService();
        directionsDisplay = new google.maps.DirectionsRenderer();
        directionsDisplay.setMap(map);

        google.maps.event.addListenerOnce(map, 'idle', function() {
          enableMap();
        });
      }
    }

    function enableMap() {

      $rootScope.enableMap = true;
    }

    function disableMap() {
      $rootScope.enableMap = false;
    }

    function loadGoogleMaps() {
      // This function will be called once the SDK has been loaded
      $window.mapInit = function() {
        initMap();
      };

      // Create a script element to insert into the page
      var script = $document[0].createElement("script");
      script.type = "text/javascript";
      script.id = "googleMaps";

      // Note the callback function in the URL is the one we created above
      if (apiKey) {
        script.src = 'https://maps.google.com/maps/api/js?key=' + apiKey + '&sensor=true&callback=mapInit';
      } else {
        script.src = 'https://maps.google.com/maps/api/js?sensor=true&callback=mapInit';
      }
      $document[0].body.appendChild(script);
    }

    function checkLoaded() {
      if (typeof google == "undefined" || typeof google.maps == "undefined") {
        $timeout(function() {
          loadGoogleMaps();
        }, 2000);
      } else {
        enableMap();
      }
    }

    function addRoute(origin, destination, waypts, optimizeWaypts) {
      routeResponse = null;
      if (typeof google !== "undefined") {
        var routeRequest = {
          origin: origin,
          destination: destination,
          waypoints: waypts,
          optimizeWaypoints: optimizeWaypts,
          travelMode: google.maps.TravelMode.DRIVING
        };

        directionsService.route(routeRequest, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            google.maps.event.trigger(map, 'resize');

            routeResponse = response;

            $rootScope.$broadcast('googleRouteCallbackComplete');
          }
        });
      }
    }

    function removeRoute() {
      if (typeof google !== "undefined" && typeof directionsDisplay !== "undefined") {
        directionsDisplay.setMap(null);
        directionsDisplay = null;
        directionsDisplay = new google.maps.DirectionsRenderer();
        directionsDisplay.setMap(map);
      }
    }

    return {
      initService: function(mapEl, key) {
        initService(mapEl, key);
      },
      checkLoaded: function() {
        checkLoaded();
      },
      disableMap: function() {
        disableMap();
      },
      removeRoute: function() {
        removeRoute();
      },
      getRouteResponse: function() {
        return routeResponse;
      },
      addRoute: function(origin, destination, waypts, optimizeWaypts) {
        addRoute(origin, destination, waypts, optimizeWaypts);
      }
    };

  }])


  .factory('ConnectivityService', [function() {
    return {
      isOnline: function() {
        var status = localStorage.getItem('networkStatus');
        if (status === null || status == "online") {
          return true;
        } else {
          return false;
        }
      }
    };
  }])
  .factory('NetworkService', ['GoogleMapsService', function(GoogleMapsService) {
    return {
      networkEvent: function(status) {
        var pastStatus = localStorage.getItem('networkStatus');
        if (status == "online" && pastStatus != status) {


          GoogleMapsService.checkLoaded();
        }
        if (status == "offline" && pastStatus != status) {

          GoogleMapsService.disableMap();
        }
        localStorage.setItem('networkStatus', status);
        return true;
      }
    };
  }])

  .controller('MapCtrl', function($scope, $rootScope, $timeout, GoogleMapsService) {

    // Ideally, this initService should be called on page load and before code below is run
    $scope.init = function () {
      GoogleMapsService.initService(document.getElementById("map"));
    };

    $scope.journeyLegs = [];
    var journeyLeg = {
      "zipPostalCode": "66045",
      "contactId": "1"
    };
    $scope.journeyLegs.push(journeyLeg);


    $scope.addRoute = function(origin, destination) {
      if (origin !== "" && destination !== "") {


        var waypts = [];
        for (i = 0, len = $scope.journeyLegs.length; i < len; i++) {
          waypts.push({
            location: $scope.journeyLegs[i].zipPostalCode,
            stopover: true
          });
        }
        GoogleMapsService.addRoute(origin, destination, waypts, true);
      }
    };


    var deregisterUpdateDistance = $rootScope.$on('googleRouteCallbackComplete', function(event, args) {
      $scope.updateDistance();
    });



    $timeout(function() {

      $scope.addRoute("94087", "27215");
    },3000);


    $scope.$on('$destroy', function() {
      deregisterUpdateDistance();
    });

    $scope.updateDistance = function() {
      $timeout(function() {

        var routeResponse = GoogleMapsService.getRouteResponse();
        if (routeResponse) {

          var route = routeResponse.routes[0];

          var distance = route.legs[route.legs.length - 1].distance;
          var duration = route.legs[route.legs.length - 1].duration;

          console.log("distance.value = ", distance.value);
          console.log("duration.value = ", duration.value);
          console.log("duration.text = ", duration.text);
        }
      });
    };

  });
